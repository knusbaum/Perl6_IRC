use v6;

#use Grammar::Tracer;

grammar IRCParser::Message::Grammar {
    token TOP { <message> }
    regex message { [":" <prefix> \s+]? <command> <params>? <crlf> }
    regex prefix { <servername>|[<nickname> ["!" <user>]? ["@" <host>]?] }
    token command { <:L>+|[\d\d\d] }
    #token params { \s+ [ ':' <trailing> | <middle> <params>]? }
    regex params { [<mparam> <eparam>]? }
    token mparam { [\s+ <middle>]* }
    token eparam { \s+ ':' <trailing> }
    token middle { <-[\s\r\n:]> <-[\s\r\n]>* }
    token trailing { <-[\r\n]>* }
    token crlf { \r\n }
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    token nickname { <:L + [_-]>+ }
    token user { <:L + [~\-_]>+ }
    token host { <:L + [\d\/.-]>+ }
    token servername { <host> }
}
 
