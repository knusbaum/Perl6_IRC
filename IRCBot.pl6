use v6;

use IRCParser;
use IRCParser::Bot;

my $nick = "nick";

my $bot = IRCParser::Bot.new(
    host => "195.148.124.79", 
    port => 6667,
    nick => $nick,
    user => "realUser"
    );

$bot.add_handler(
    "PING", 
    -> $bot, $mess {
        say $mess<message><params>.Str;
        $bot.sock_send("PONG" ~ $mess<message><params>.Str);
        say "Pong'd.";
    });

$bot.add_handler(
    "NOTICE",
    -> $bot, $mess {
        say $mess<message><params>.Str;
    });

$bot.add_handler(
    "PRIVMSG",
    -> $bot, $mess {
        my $privmsg = $mess<message><params><eparam><trailing>.Str;
        if ($privmsg ~~ /^$nick <[:,\s]>*/) {
            $privmsg ~~ s/^$nick <[:,\s]>*//;
            $bot.run_function($mess, $privmsg.split(' '));
        }
    });

$bot.add_handler("numeric", -> $bot, $mess { say $mess<message>.Str.trim; });

$bot.add_handler(
    "QUIT",
    -> $bot, $mess {
        say $mess<message><prefix><user> ~ " has Quit.";
    });

$bot.add_function(
    "help",
    -> $mess, @params, $bot, $from {
        my $msg = "Available functions: ";
        map -> $v { $msg ~= "$v " }, $bot.functions.keys;
        $bot.send_privmsg($from, $msg);
    });

$bot.add_function(
    "add",
    -> $mess, @params, $bot, $from {
        my $answer = reduce -> $x, $y { $x + $y }, 0, @params;
        $bot.send_privmsg($from, $answer);
    });

$bot.run;
