use v6;

use IRCParser;

class IRCParser::Bot::From {
    has $.channel;
    has $.user;
    
    submethod BUILD(:$channel, :$user) {
        $!channel = $channel;
        $!user = $user;
    }
}

class IRCParser::Bot {
    has IO::Socket::INET $!socket;
    has %!handlers;
    has %.functions;
    
    has $!nick;
    has $!user;

    submethod BUILD(:$host, :$port, :$nick, :$user) {
        $!socket = IO::Socket::INET.new(
            host => $host, 
            port => $port,
            input-line-separator => "\r\n"
        );
        $!nick = $nick;
        $!user = $user;
    }

    method add_handler($name, &fun) {
        %!handlers{$name} = &fun;
    }

    method add_function($name, &fun) {
        %.functions{$name} = &fun;
    }

    method run() {
        self.sock_send("NICK $!nick");
        self.sock_send("USER $!user 0 * :myRealName");
        self.sock_send("JOIN ##some");
        
        while (my $i = $!socket.get) {

            my $mess = IRCParser::Message::Grammar.parse($i ~ "\r\n");
            if ($mess) {

                if (~$mess<message><command>.Str ~~ /\d\d\d/ 
                    && %!handlers{"numeric"}) {
                    %!handlers{"numeric"}(self, $mess);
                }
                elsif (%!handlers{~$mess<message><command>.Str}) {
                    %!handlers{~$mess<message><command>.Str}(self, $mess);
                }
                else {
                    say "uNo handler for: " ~ $mess<message>.Str;
                }
            }
        }
    }

    method run_function($mess, @params) {
        say "Got: "  ~ $mess.trim;
        if (%.functions{@params[0]}) {
            my $channel = $mess<message><params><mparam><middle>.Str;
            my $user = $mess<message><prefix><nickname>.Str;
            my $from = IRCParser::Bot::From.new(
                channel => $channel,
                user => $user
                );
            %.functions{@params[0]}($mess, @params[1..Inf], self, $from);
        }
        else {
            my $channel = $mess<message><params><mparam><middle>.Str;
            my $user = $mess<message><prefix><nickname>.Str;
            my $msg = "PRIVMSG $channel :Sorry, $user, I don't understand.";
            self.sock_send($msg);
        }
    }

    method send_privmsg($from, $str) {
        my $channel = $from.channel;
        my $user = $from.user;
        my $msg = "PRIVMSG $channel :$user: $str";
        self.sock_send($msg);
    }

    method sock_send(Str $str) {
        say "Sending: $str";
        $!socket.send($str ~ "\r\n");
    }
}

